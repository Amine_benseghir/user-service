FROM openjdk:8
ADD target/UserService.jar UserService.jar
EXPOSE 8085
ENTRYPOINT ["java","-jar", "UserService.jar"]