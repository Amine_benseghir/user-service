package com.EFood.UserService.repository;

import com.EFood.UserService.entity.User;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;


@NoRepositoryBean
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
}
