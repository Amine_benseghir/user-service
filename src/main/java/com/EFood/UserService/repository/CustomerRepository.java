package com.EFood.UserService.repository;

import com.EFood.UserService.entity.Customer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends UserRepository {

    @Query("SELECT p FROM Customer p WHERE LOWER(p.lastName) = LOWER(:lastName)")
     Optional<Customer> findByName(@Param("lastName") String lastName);
}
