package com.EFood.UserService.repository;

import com.EFood.UserService.entity.Employee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeRepository extends UserRepository {

    @Query("SELECT p FROM Employee p WHERE LOWER(p.lastName) = LOWER(:lastName)")
    Optional<Employee> findByName(@Param("lastName") String lastName);
}
