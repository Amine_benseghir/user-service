package com.EFood.UserService.exception;

public class RessourceNotFoundException extends RuntimeException {
    public RessourceNotFoundException(String message){
        super(message);
    }
    public RessourceNotFoundException(String message,Throwable throwable){
        super(message,throwable);
    }
}
