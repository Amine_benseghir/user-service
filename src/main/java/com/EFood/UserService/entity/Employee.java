package com.EFood.UserService.entity;

import javax.persistence.Entity;

@Entity
public class Employee extends User {


    private String position;
    private Double Salary;



    public Employee(String firstName, String lastName, String email, String gender, Integer age, String position, Double salary) {
        super(firstName, lastName, email, gender, age);
        this.position = position;
        Salary = salary;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Double getSalary() {
        return Salary;
    }

    public void setSalary(Double salary) {
        Salary = salary;
    }
}
