package com.EFood.UserService.entity;

import javax.persistence.Entity;

@Entity
public class Customer extends User {
    public Customer() {
    }

    public Customer(String firstName, String lastName, String email, String gender, Integer age, String city) {
        super(firstName, lastName, email, gender, age);
        this.city = city;
    }

    private String city;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
