package com.EFood.UserService.service;

import com.EFood.UserService.entity.Employee;
import com.EFood.UserService.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

public interface IEmployeeDao  {

    public Employee createEmployee(Employee employee);
    public Page<User> findAll(Pageable pageNumber);
    public Employee findByName(String lastName);
    public void deleteEmployee(long id);
    public Employee findById(Long idEmployee);
    public Page<User> SortByAge(int pageNumber,int age);
    public Page<User> sortBySalary(int pageNumber,int salary);
    public Employee updateEmployee(Employee employee);

}
