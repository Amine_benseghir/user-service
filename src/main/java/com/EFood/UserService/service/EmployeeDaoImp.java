package com.EFood.UserService.service;

import com.EFood.UserService.exception.RessourceNotFoundException;
import com.EFood.UserService.repository.EmployeeRepository;

import com.EFood.UserService.entity.Employee;
import com.EFood.UserService.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EmployeeDaoImp implements IEmployeeDao {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Employee createEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public Page<User> findAll(Pageable pageNumber) {
        Pageable pageable = PageRequest.of( 0 , 20);
        return employeeRepository.findAll(pageable);
    }

    @Override
    public Employee findByName(String lastName) {
        Optional<Employee> customerOptional = this.employeeRepository.findByName(lastName);
        if (customerOptional.isPresent()) {
            return (Employee) customerOptional.get();
        } else {
            throw new RessourceNotFoundException("record not found");
        }

         }

    @Override
    public void deleteEmployee(long id) {


        Optional<User> employeeOptional = this.employeeRepository.findById(id);
        if (employeeOptional.isPresent()) {
            this.employeeRepository.delete(employeeOptional.get());
        } else {
            throw new RessourceNotFoundException("Record not found" + id);
        }
        employeeRepository.deleteById(id);
    }

    @Override
    public Employee findById(Long idEmployee) {
        Optional<User> employeeOptional = this.employeeRepository.findById(idEmployee);
        if (employeeOptional.isPresent()) {
            return (Employee) employeeOptional.get();
        } else {
            throw new RessourceNotFoundException("record not found");
        }
    }

    @Override
    public Page<User> SortByAge(int pageNumber, int age) {
        Sort sort = Sort.by("age").descending();
        Pageable pageable = PageRequest.of(pageNumber - 1, 6, sort);
        return employeeRepository.findAll(pageable);
    }

    @Override
    public Page<User> sortBySalary(int pageNumber, int salary) {
        Sort sort = Sort.by("salary").descending();
        Pageable pageable = PageRequest.of(pageNumber - 1, 6, sort);
        return employeeRepository.findAll(pageable);
    }

    @Override
    public Employee updateEmployee(Employee employee) {

        Optional<User> employeeOptional = employeeRepository.findById(employee.getId());
        if (employeeOptional.isPresent()) {
            Employee employeeUpdate = (Employee) employeeOptional.get();
            employeeUpdate.setAge(employee.getAge());
            employeeUpdate.setEmail(employee.getEmail());
            employeeUpdate.setGender(employee.getGender());
            employeeUpdate.setId(employee.getId());
            employeeUpdate.setFirstName(employee.getFirstName());
            employeeUpdate.setLastName(employee.getLastName());
            employeeUpdate.setPosition(employee.getPosition());
            employeeUpdate.setSalary(employee.getSalary());
            employeeRepository.save(employee);
            return employeeUpdate;
        } else {
            throw new RessourceNotFoundException("User not Found with");
        }
    }
}