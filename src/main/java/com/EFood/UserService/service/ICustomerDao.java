package com.EFood.UserService.service;

import com.EFood.UserService.entity.Customer;
import com.EFood.UserService.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


public interface ICustomerDao {

    public Customer createCustomer(Customer customer);
    public Page<User> findAll(Pageable page);
    public void deleteCustomer(Long id);
    public Customer findByName(String lastName);
    public Customer findById(Long idCustomer);
    public Customer updateCustomer(Customer customer);

}
