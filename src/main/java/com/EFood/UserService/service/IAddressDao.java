package com.EFood.UserService.service;

import com.EFood.UserService.entity.Address;
import org.springframework.stereotype.Service;


public interface IAddressDao  {

    public Address addAddress(Address address);
    public void deleteAddress(Address address, Long idAddress, Long idUser);

}
