package com.EFood.UserService.service;

import com.EFood.UserService.exception.RessourceNotFoundException;
import com.EFood.UserService.repository.CustomerRepository;
import com.EFood.UserService.entity.Customer;
import com.EFood.UserService.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class CustomerDaoImp implements ICustomerDao {

    @Autowired
    private CustomerRepository customerRepository;



    @Override
    public Customer createCustomer(Customer customer)
    {
        return customerRepository.save(customer);
    }

    @Override
    public Page<User> findAll(Pageable page) {
        Pageable pageable = PageRequest.of(5, 20);
        return customerRepository.findAll(pageable);
    }

    @Override
    public void deleteCustomer(Long id) {
        Optional<User> customerOptional= this.customerRepository.findById(id);
        if (customerOptional.isPresent()){
            this.customerRepository.delete(customerOptional.get());
        }else {
            throw new RessourceNotFoundException("Record not found"+id);
        }

        customerRepository.deleteById(id);
        }


    @Override
    public Customer findByName(String lastName) {
        Optional<Customer> customerOptional = this.customerRepository.findByName(lastName);
        if (customerOptional.isPresent()) {
            return (Customer) customerOptional.get();
        } else {
            throw new RessourceNotFoundException("record not found");
        }

    }

    @Override
    public Customer findById(Long idCustomer) {
        Optional<User> customerOptional = this.customerRepository.findById(idCustomer);
        if (customerOptional.isPresent()) {
            return (Customer) customerOptional.get();
        } else {
            throw new RessourceNotFoundException("record not found");
        }
    }

    @Override
    public Customer updateCustomer(Customer customer) {
        Optional<User> customerOptional = this.customerRepository.findById(customer.getId());
        if (customerOptional.isPresent()) {
            Customer customerUpdate = (Customer) customerOptional.get();
            customerUpdate.setAge(customer.getAge());
            customerUpdate.setEmail(customer.getEmail());
            customerUpdate.setGender(customer.getGender());
            customerUpdate.setId(customer.getId());
            customerUpdate.setFirstName(customer.getFirstName());
            customerUpdate.setLastName(customer.getLastName());
            customerUpdate.setCity(customer.getCity());
            customerRepository.save(customer);
            return customerUpdate;
        } else {
            throw new RessourceNotFoundException("User not Found with" + customer.getFirstName() + customer.getLastName());
        }
    }
}
