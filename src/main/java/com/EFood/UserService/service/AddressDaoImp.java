package com.EFood.UserService.service;

import com.EFood.UserService.repository.AddressRepository;
import com.EFood.UserService.entity.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressDaoImp implements IAddressDao {

    @Autowired
    private AddressRepository addressRepository;

    @Override
    public Address addAddress(Address address) {
        return (Address) addressRepository.save(address);
    }

    @Override
    public void deleteAddress(Address address, Long idAddress, Long idUser) {

        if (idAddress.equals(idUser)){
            addressRepository.delete(address);
        }
    }
}
