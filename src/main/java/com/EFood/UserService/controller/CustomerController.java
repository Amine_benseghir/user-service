package com.EFood.UserService.controller;

import com.EFood.UserService.service.ICustomerDao;
import com.EFood.UserService.entity.Customer;
import com.EFood.UserService.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/customers")

public class CustomerController {

    @Autowired
    private ICustomerDao iCustomerDao;

    @PostMapping("/addCustomer")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity addCustomer(@RequestBody Customer customer) {
        return ResponseEntity.ok(iCustomerDao.createCustomer(customer));

    }

    @GetMapping
    public ResponseEntity<Page<User>> findAll(Pageable pageNumber) {
        return ResponseEntity.ok(iCustomerDao.findAll(pageNumber));
    }

    @GetMapping("/{name}")
    public ResponseEntity<Customer>findByName(@PathVariable String name){
        return ResponseEntity.ok(iCustomerDao.findByName(name));
    }

    @GetMapping("/{id}")
    public ResponseEntity <Customer>findById(@PathVariable Long id){
        return ResponseEntity.ok(iCustomerDao.findById(id));
    }


    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        iCustomerDao.deleteCustomer(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Customer>update(@PathVariable Long id ,@RequestBody Customer customer){
        return ResponseEntity.ok(iCustomerDao.updateCustomer(customer));
    }

}