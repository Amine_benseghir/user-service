package com.EFood.UserService.controller;

import com.EFood.UserService.service.IEmployeeDao;
import com.EFood.UserService.entity.Employee;
import com.EFood.UserService.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/employees")
public class EmployeeController {

    @Autowired
    private IEmployeeDao iEmployeeDao;

    @PostMapping("addEmployee")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity addUser(@RequestBody Employee employee) {
        return ResponseEntity.ok(iEmployeeDao.createEmployee(employee));
    }

    @GetMapping
    public ResponseEntity<Page<User>> findAll(Pageable pageNumber) {
        return ResponseEntity.ok(iEmployeeDao.findAll(pageNumber));

    }

    @GetMapping("/{name}")
    public ResponseEntity<Employee>findByName(@PathVariable String name){
        return ResponseEntity.ok(iEmployeeDao.findByName(name));
    }
    @GetMapping("/{id}")
    public ResponseEntity <Employee>findById(@PathVariable Long id){
        return ResponseEntity.ok(iEmployeeDao.findById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        iEmployeeDao.deleteEmployee(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Employee>update(@PathVariable Long id ,@RequestBody Employee employee){
        return ResponseEntity.ok(iEmployeeDao.updateEmployee(employee));
    }

}
