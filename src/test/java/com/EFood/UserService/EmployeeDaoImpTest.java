package com.EFood.UserService;


import com.EFood.UserService.service.IEmployeeDao;
import com.EFood.UserService.entity.Employee;
import com.EFood.UserService.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.Rollback;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class EmployeeDaoImpTest {

    @Autowired
    private IEmployeeDao employeeDao;

    @Test
    @Rollback(false)
    void testCreateEmployee() {
        Employee employee = new Employee("Amine", "Benseghir", "aa@hotmail.com", "male", 24, "manager", 5000.00);
        Employee createEmployee = employeeDao.createEmployee(employee);
        assertNotNull(createEmployee);
    }

    @Test
    @Rollback(false)
    void testFindfAll() {
        int pageNumber = 5;
        int size = 6;
        Pageable pageable = PageRequest.of(pageNumber, size);
        Page<User> employees = employeeDao.findAll(pageable);
        assertNotNull(employees);
    }

    @Test
    @Rollback(false)
    void testFindById(){

        Long id = 1L;

        Employee employee=employeeDao.findById(id);
        assertNotNull(employee);
    }

    @Test
    @Rollback(false)
    void TestDeleteEmployee(Long id) {
        Employee employee=employeeDao.findById(2L);
        employeeDao.deleteEmployee(employee.getId());
        Employee employee1 = employeeDao.findById(2L);
        assertNull(employee1);
    }
    @Test
    @Rollback(false)
    void TestupdateCustomer(){
        Employee employee = employeeDao.findById(3L);
        employee.setFirstName("sqsq");
        employeeDao.createEmployee(employee);
        employeeDao.findById(3L);
        assertEquals(employee.getFirstName(),"sqsq");
    }
    @Test
    @Rollback(false)
    void findByName(){

    }

}