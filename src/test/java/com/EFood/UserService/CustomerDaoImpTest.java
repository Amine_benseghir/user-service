package com.EFood.UserService;

import com.EFood.UserService.service.ICustomerDao;
import com.EFood.UserService.entity.Customer;
import com.EFood.UserService.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.Rollback;

import static org.junit.jupiter.api.Assertions.*;


@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)

public class CustomerDaoImpTest {

    @Autowired
    private ICustomerDao customerDao;



    @Test
    @Rollback(false)
    void  testCreateCustomer(){
        Customer customer = new Customer("amine","benseghir","aa@aa.com","male",30,"sz");
          Customer saveCustomer = customerDao.createCustomer((Customer) customer);
          assertNotNull(saveCustomer);
    }
    @Test
    @Rollback(false)
    void testFindfAll(){
        int pageNumber = 5;
        int size = 6;
        Pageable pageable = PageRequest.of(pageNumber, size);
        Page<User> customers = customerDao.findAll(pageable);
        assertNotNull(customers);
    }
    @Test
    @Rollback(false)
    void testFindById(){
        Long id = 2L;
         Customer customer=customerDao.findById(id);
         assertNotNull(customer);

          }

    @Test
    @Rollback(false)
    void TestdeleteCustomer(Long id) {
        Customer customer=customerDao.findById(2L);
         customerDao.deleteCustomer(customer.getId());
         Customer customer1 = customerDao.findById(2L);
              assertNull(customer1);
    }
    @Test
    @Rollback(false)
    void TestupdateCustomer(){
        Customer customer = customerDao.findById(3L);
        customer.setFirstName("sqsq");
        customerDao.createCustomer(customer);
        customerDao.findById(3L);
        assertEquals(customer.getFirstName(),"sqsq");
    }
      @Test
      @Rollback(false)
      void findByName(){

    }




}
