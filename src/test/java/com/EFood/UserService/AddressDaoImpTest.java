package com.EFood.UserService;

import com.EFood.UserService.service.IAddressDao;
import com.EFood.UserService.entity.Address;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class AddressDaoImpTest {

    private IAddressDao iAddressDao;

    @Test
    void addAddress(){

        Address address = new Address("office","77 avenue ouazane Belksiri");
        Address address1=iAddressDao.addAddress(address);
        assertNotNull(address1);
    }
    @Test
    void  deleteAddress(Long idAddress,Long idUser){
        Address address = new Address("home","azert, fssq");
                     Address address2 = iAddressDao.addAddress(address);
                     iAddressDao.deleteAddress(address2,1L,2L);
    }
}
